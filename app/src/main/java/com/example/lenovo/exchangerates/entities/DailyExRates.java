package com.example.lenovo.exchangerates.entities;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "DailyExRates")
public class DailyExRates {

    private String date;
    private ArrayList<Currency> currency;

    @Attribute(name = "Date")
    public String getDate() {
        return date;
    }

    @Attribute(name = "Date")
    public void setDate(String date) {
        this.date = date;
    }

    @ElementList(inline = true, name = "Currency")
    public ArrayList<Currency> getCurrency() {
        return currency;
    }

    @ElementList(inline = true, name = "Currency")
    public void setCurrency(ArrayList<Currency> currency) {
        this.currency = currency;
    }
}
