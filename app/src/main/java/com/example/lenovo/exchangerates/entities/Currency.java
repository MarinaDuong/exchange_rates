package com.example.lenovo.exchangerates.entities;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Currency")
public class Currency {

    private String id;
    private String name;
    private String charCode;
    private int numCode;
    private int scale;
    private float rate;

    @Attribute(name = "Id")
    public String getId() {
        return id;
    }

    @Attribute(name = "Id")
    public void setId(String id) {
        this.id = id;
    }

    @Element(name = "Name")
    public String getName() {
        return name;
    }

    @Element(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    @Element(name = "CharCode")
    public String getCharCode() {
        return charCode;
    }

    @Element(name = "CharCode")
    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    @Element(name = "NumCode")
    public int getNumCode() {
        return numCode;
    }

    @Element(name = "NumCode")
    public void setNumCode(int numCode) {
        this.numCode = numCode;
    }

    @Element(name = "Scale")
    public int getScale() {
        return scale;
    }

    @Element(name = "Scale")
    public void setScale(int scale) {
        this.scale = scale;
    }

    @Element(name = "Rate")
    public float getRate() {
        return rate;
    }

    @Element(name = "Rate")
    public void setRate(float rate) {
        this.rate = rate;
    }

}
