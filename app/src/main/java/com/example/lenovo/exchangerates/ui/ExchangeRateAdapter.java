package com.example.lenovo.exchangerates.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.exchangerates.R;
import com.example.lenovo.exchangerates.entities.Currency;

import java.util.ArrayList;

public class ExchangeRateAdapter extends RecyclerView.Adapter<ExchangeRateAdapter.ExRateViewHolder> {

    private ArrayList<Currency> currencies;
    private Context context;

    public ExchangeRateAdapter(Context context, ArrayList<Currency> currencies) {
        this.context = context;
        this.currencies = currencies;
    }

    @Override
    public ExRateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.exrate_item, parent, false);
        return new ExRateViewHolder(listRow);
    }

    @Override
    public void onBindViewHolder(ExRateViewHolder holder, int position) {
        Currency currency = currencies.get(position);
        holder.currency.setText(context.getResources().getString(R.string.currency, currency.getCharCode(), currency.getRate(), currency.getName(), currency.getScale()));
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    public class ExRateViewHolder extends RecyclerView.ViewHolder {
        TextView currency;

        private ExRateViewHolder(View itemView) {
            super(itemView);
            currency = itemView.findViewById(R.id.currency_tv);
        }
    }
}
