package com.example.lenovo.exchangerates.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.lenovo.exchangerates.R;
import com.example.lenovo.exchangerates.entities.Currency;
import com.example.lenovo.exchangerates.entities.DailyExRates;
import com.example.lenovo.exchangerates.rest.ExchangeRatesRestClient;
import com.example.lenovo.exchangerates.utils.Helpers;

import java.util.ArrayList;

public class ExchangeRatesActivity extends AppCompatActivity {
    private ExchangeRatesRestClient exchangeRatesRestClient;
    private ArrayList<Currency> exRate = new ArrayList<Currency>();
    private RecyclerView recyclerView;
    private ExchangeRateAdapter exchangeRateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_rates);

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        exchangeRateAdapter = new ExchangeRateAdapter(getApplicationContext(), exRate);
        recyclerView.setAdapter(exchangeRateAdapter);

        if (Helpers.hasConnection(getApplicationContext())) {
            exchangeRatesRestClient = new ExchangeRatesRestClient();
            loadMoreExRates();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
        }

        Helpers.recyclerDrag(exRate, exchangeRateAdapter, recyclerView);

    }

    private void loadMoreExRates() {
        exchangeRatesRestClient.getExchangeRates(new ExchangeRatesRestClient.ResultListener<DailyExRates>() {
            @Override
            public void onSuccess(DailyExRates result) {
                if (result != null) {
                    exRate.addAll(result.getCurrency());
                    recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onConnectionFailure(String message) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

}
