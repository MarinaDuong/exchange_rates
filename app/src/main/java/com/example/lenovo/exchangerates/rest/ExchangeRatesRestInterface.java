package com.example.lenovo.exchangerates.rest;

import com.example.lenovo.exchangerates.entities.DailyExRates;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ExchangeRatesRestInterface {

    @GET("/Services/XmlExRates.aspx")
    Call<DailyExRates> getExchangeRates();
}
