package com.example.lenovo.exchangerates.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.lenovo.exchangerates.entities.Currency;
import com.example.lenovo.exchangerates.ui.ExchangeRateAdapter;

import java.util.ArrayList;

import me.rishabhkhanna.recyclerswipedrag.RecyclerHelper;

public class Helpers {
    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //get all networks information
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            int i;

            //checking internet connectivity
            for (i = 0; i < networks.length; ++i) {
                if (cm.getNetworkInfo(networks[i]).getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
            return false;
        } else {
            NetworkInfo wifiInfo = cm.getActiveNetworkInfo();
            return wifiInfo != null;
        }
    }

    public static void recyclerDrag(ArrayList<Currency> arrayList, ExchangeRateAdapter exchangeRateAdapter, RecyclerView recyclerView) {
        RecyclerHelper touchHelper = new RecyclerHelper<Currency>(arrayList, (RecyclerView.Adapter) exchangeRateAdapter);
        touchHelper.setRecyclerItemDragEnabled(true);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(touchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
}
