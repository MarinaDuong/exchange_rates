package com.example.lenovo.exchangerates.rest;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.lenovo.exchangerates.entities.DailyExRates;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExchangeRatesRestClient {

    public void getExchangeRates(final ResultListener<DailyExRates> resultListener) {
        ExchangeRatesRestService.getExchangeRatesRestService().getExchangeRates().enqueue(new Callback<DailyExRates>() {
            @Override
            public void onResponse(Call<DailyExRates> call, Response<DailyExRates> response) {
                //при response.isSuccessful() true code() берется [200 ..300). Я считаю, что правильный ответ приходит только при code()= 200, а при остальных кодах получаем текст ошибки
                if (response.code() == 200) {
                    resultListener.onSuccess(response.body());
                } else {
                    resultListener.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<DailyExRates> call, Throwable t) {
                resultListener.onConnectionFailure(t.getMessage());
            }
        });
    }

    public static abstract class ResultListener<T> {
        public abstract void onSuccess(T result);

        public abstract void onConnectionFailure(String message);

        public abstract void onError(String errorMessage);
    }
}
