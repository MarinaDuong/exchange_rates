package com.example.lenovo.exchangerates.rest;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ExchangeRatesRestService {

    public static ExchangeRatesRestInterface getExchangeRatesRestService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.nbrb.by")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        exchangeRatesRestInterface = retrofit.create(ExchangeRatesRestInterface.class);
        return exchangeRatesRestInterface;
    }

    private static ExchangeRatesRestInterface exchangeRatesRestInterface;

    private ExchangeRatesRestService(){

    }
}
